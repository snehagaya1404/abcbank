<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ page import="java.util.ArrayList"%>
<%@ page import="com.abc.transaction.Transaction"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<jsp:include page="link.jsp"></jsp:include>
<title>Transaction Records</title>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
	<div class="container">
	<h3>Transaction Record</h3>
			<table class="table">
				<thead>
					<tr>
						<th>Account Number</th>
						<th>Account Holder Name</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
				<% for(Transaction t : ((ArrayList)(session.getAttribute("record")))){%>
					<tr><% out.println(t.getTransactionAccountNo()); %></tr>
					<tr><% out.println(t.getTransactionHolderName()); %></tr>
					<tr><% out.println(t.getAmount()); %></tr>
				<%}%>
				</tbody>
			</table>
		</div>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>