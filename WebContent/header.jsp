
<%
	if (session.getAttribute("username") == null) {
%>
<script type="text/javascript">
	window.location = "login.jsp";
</script>
/* response.sendRedirect("/ABCBank/login.jsp"); */
<%
	session.setAttribute("error", "<div class='alert alert-danger' role='alert'>" + "You must login first...."
				+ "</div>");
	}
%>
<div>
	<div class="container-fluid ">
		<div class="row">
			<div class="col-lg-3 bg-dark text-white" style="height: 60px">
				<img src="assests/images/logo.jpg" alt="Logo" width="300" height="75">
			</div>
			<div class="col-lg-9 bg-dark text-right">
				<a href="/ABCBank/logout.jsp" class="pt-2 text-white">Logout</a>
			</div>
		</div>

		<div class="row h-100 ">
			<div class="col-lg-3 bg-dark " style="height: 597px">
				<ul class="nav flex-column nav-tabs">
					<li class="nav-item"><a class="nav-link text-white"
						href="/ABCBank/home.jsp">Home</a><br></li>
					<li class="nav-item">
						<!--  <i class="fa fa-search text-white">--> <a
						class="nav-link text-white " href="BalanceController">View
							Balance</a><br> <!-- </i> -->

					</li>
					<li class="nav-item"><a class="nav-link text-white"
						href="/ABCBank/transfer.jsp">Transfer Money</a><br></li>
					<li class="nav-item"><a class="nav-link text-white"
						href="/ABCBank/changePassword.jsp">Change Password</a><br></li>
					<li class="nav-item"><a class="nav-link text-white"
						href="/ABCBank/TransactionController"><!-- Transaction record--></a><br>
					</li>
				</ul>
			</div>
			<div class="col-lg-9">