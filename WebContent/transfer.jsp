<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Transaction Layout</title>
<jsp:include page="link.jsp"></jsp:include>

</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-7 col-sm-12">
				<form action="TransferController" method="post" class="mt-5 ml-4">
				<h3>Enter Receiver details:</h3><br>
				<!-- account doesn't exist case -->
				<% if(session.getAttribute("error1")!=null)	
				{%>
					<div class="alert alert-danger">
						<%out.println(session.getAttribute("error1")); %>
					</div>
					<%session.removeAttribute("error1"); 
				}%>
				
				<!-- amount must be greater than 0 case -->
				<% if(session.getAttribute("error2")!=null)	
				{%>
					<div class="alert alert-danger">
						<%out.println(session.getAttribute("error2")); %>
					</div>
					<%session.removeAttribute("error2"); 
				}%>
				
				<!-- insufficient balance case -->
				<% if(session.getAttribute("error3")!=null)	
				{%>
					<div class="alert alert-danger">
						<%out.println(session.getAttribute("error3")); %>
					</div>
					<%session.removeAttribute("error3"); 
				}%>
				
				<!-- successful transaction -->
				<% if(session.getAttribute("success")!=null)	
				{%>
					<div class="alert alert-success">
						<%out.println(session.getAttribute("success"));%>
						<br>
						Available balance:<% out.println(session.getAttribute("balance"));%>
						
					</div>
					<%session.removeAttribute("success"); 
					 session.removeAttribute("balance");
				}%>
				
				
					<div class="form-group">
							<label>Account Number</label>
							<input type="text" name="account_number" class="form-control" placeholder="Enter receiver account number">
					</div>
					<div class="form-group">
						<label>Account Holder Name</label>
						<input type="text" name="account_holder_name" class="form-control" placeholder="Enter name">
					</div>
					<div class="form-group">
						<label>Amount</label>
						<input type="text" name="amount" class="form-control" placeholder="Enter amount">
					</div>
					
					<button type="submit" class="btn btn-dark btn-block">Submit</button>
				</form>
			</div>
		</div>
	</div>	
				
			
<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>