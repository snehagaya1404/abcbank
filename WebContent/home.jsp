<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Homepage</title>

<jsp:include page="link.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>	
				
				<h1 class="py-3 pl-2">Welcome <% out.println(session.getAttribute("username")); %>!</h1>
					
      <jsp:include page="footer.jsp"></jsp:include>

</body>
</html>