<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login Page</title>
<jsp:include page="link.jsp"></jsp:include>
<!-- <script src="assests/js/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="assests/js/bootstrap-show-password.min.js" type="text/javascript"></script> -->
<script src="https://kit.fontawesome.com/a241cceef5.js" crossorigin="anonymous"></script><!-- font awesome -->
<style type="text/css">
	.form-container{
		padding: 40px;
		border-radius: 10px;
		box-shadow: 0px 0px 10px 0px #000;
	}
		
	
</style>
 
</head>
<body>
	
	<div class="container-fluid mt-5">
		<div class="row justify-content-center">
			<div class="col-12 col-sm-6 col-md-4">
				
				
				<form action="LoginController" method="post" class="form-container">
					<h3 class="text-center">ABC Bank Login</h3><br>  <!-- include font -->
					<%if(session.getAttribute("error") != null)
					{
						out.println(session.getAttribute("error"));
						session.removeAttribute("error");
					}%>
					<div class="form-group">
						<label><b>Username</b></label><br>
						<input type="text" id="username" name = "username" class="form-control" placeholder="Enter Username"><br>
					</div>
					<div class="form-group">
						<label><b>Password</b></label><br>
						<div class="input-group">
							<input type="password" id="password" name = "password" class="form-control" placeholder="Enter Password" data-toggle="password">
						</div>
					</div>
					<button  type="submit" class="btn btn-dark btn-block mt-4">Login</button>
				</form>
				<!--  <a href="forgetPassword.jsp">Forget Password</a> -->
			</div>
		</div>
	</div>

</body>
</html>