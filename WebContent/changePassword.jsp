<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Change Password</title>
<jsp:include page="link.jsp"></jsp:include>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-7 col-sm-12">
				<form action="ChangePasswordController" method="post" class=" my-5 ml-4">
					<h3>Change Password:</h3><br>
					<%if(session.getAttribute("error") != null)		//if new pwd and conform pwd are not same
					{%>
						<div class="alert alert-danger" role="alert">
							<% out.println(session.getAttribute("error"));	%>				
						</div>
					<% 	session.removeAttribute("error");
					}%>
					<%if(session.getAttribute("error1") != null)	//if old password didn't match
					{%>
						<div class="alert alert-danger" role="alert">
							<% out.println(session.getAttribute("error1"));%>
						</div>
						<% session.removeAttribute("error1");
					}
					%>
					<%if(session.getAttribute("success")!=null)
					{%>
						<div class="alert alert-success" role="alert">
							<% out.println(session.getAttribute("success")); %>
						</div>
						<% session.removeAttribute("success");
					}%>
					<div class="form-group">
							<label>Old Password</label>
							<input type="password" name="oldpassword" id="oldpassword" class="form-control" placeholder="Enter old password">
					</div>
					<div class="form-group">
						<label>New Password</label>
						<input type="password" name="newpassword" id="newpassword" class="form-control" placeholder="Enter new password">
					</div>
					<div class="form-group">
						<label>Confirm New Password</label>
						<input type="password" name="confirmpassword" id="confirmpassword" class="form-control" placeholder="Re-enter new password">
					</div>
					
					<button type="submit" class="btn btn-dark btn-block">Change Password</button>
				</form>
			</div>
		</div>
	</div>	
<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>