package com.abc.balance;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.abc.model.Model;

@SuppressWarnings("serial")
public class BalanceController extends HttpServlet {
	
	public void service(HttpServletRequest req, HttpServletResponse res) 
	{
		System.out.println("in balance controller:");
		HttpSession session = req.getSession(false);
		//System.out.println(session.getAttribute("id"));
		int id = (int)session.getAttribute("id");
		Model m = new Model();
		int balance = m.getBalance(id);
		session.setAttribute("balance", balance);
		System.out.println("balance is:"+balance);
		try {
			res.sendRedirect("/ABCBank/balance.jsp");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
