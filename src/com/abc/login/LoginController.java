package com.abc.login;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.abc.model.Model;

@SuppressWarnings("serial")
public class LoginController extends HttpServlet {
	public void service(HttpServletRequest req, HttpServletResponse res)
	{
		//System.out.println("This is login");
		try {
			HttpSession session = req.getSession(true);
			String username = req.getParameter("username");
			String pwd = req.getParameter("password");
			
			/*System.out.println("name:"+name);
			System.out.println("password:"+pwd);*/
			Model m = new Model();
			m.setUsername(username);
			m.setPassword(pwd);

			if(m.validLogin()){
				session.setAttribute("username", username);/////usnername in session
				session.setAttribute("id", m.getId());////// id in session
				session.setAttribute("balance", m.getBalance());
				System.out.println("Login successfully..");
				res.sendRedirect("/ABCBank/home.jsp");
			}
			else{
				session.setAttribute("error", "<div class='alert alert-danger' role='alert'>"
						  						+"Login failed..."
											+"</div>");
				//System.out.println("Login failed..");
				res.sendRedirect("/ABCBank/login.jsp");
			
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
