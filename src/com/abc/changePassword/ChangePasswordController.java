package com.abc.changePassword;

import java.io.IOException;

import javax.servlet.http.HttpServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.abc.model.Model;


@SuppressWarnings("serial")
public class ChangePasswordController extends HttpServlet 
{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
	{
		Model m = new Model();
		HttpSession session = req.getSession(false);
		int id = (int)session.getAttribute("id");
		
		String oldpassword = (String)req.getParameter("oldpassword");
		String pwd =  m.checkOldPassword(id);
		
		if(oldpassword.equals(pwd))
		{
			String newpassword = (String) req.getParameter("newpassword");
			m.setPassword(id,newpassword);
			session.setAttribute("success", "Password Changed Sucessfully");
			try {
				res.sendRedirect("/ABCBank/changePassword.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else{
			session.setAttribute("error1", "Old Password didn't matched..");
			try {
				res.sendRedirect("/ABCBank/changePassword.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
}
