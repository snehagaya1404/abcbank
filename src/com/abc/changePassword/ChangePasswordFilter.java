package com.abc.changePassword;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ChangePasswordFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
	{
		try {
			
			if(((String)request.getParameter("newpassword")).equals((String)request.getParameter("confirmpassword"))){
				chain.doFilter(request, response);
			}
			else{
				HttpSession session = ((HttpServletRequest) request).getSession(false);
				session.setAttribute("error", "Password mismatch...");
				System.out.println("Password mismatch...");
				((HttpServletResponse) response).sendRedirect("/ABCBank/changePassword.jsp");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
