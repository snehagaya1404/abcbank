package com.abc.transfer;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.abc.model.Model;

@SuppressWarnings("serial")
public class TransferController extends HttpServlet {
	
	public void service(HttpServletRequest req , HttpServletResponse res)
	{
		HttpSession session = req.getSession(false);	

		int id_receiver = Integer.parseInt(req.getParameter("account_number"));
		String receiver_name = req.getParameter("account_holder_name");
		
		Model m = new Model();
		if( m.validateAccount(id_receiver, receiver_name))
		{
			int amount = Integer.parseInt(req.getParameter("amount"));
			if( amount > 0) //checks whether entered amount is 0
			{
				int id_sender  = (int)session.getAttribute("id");
				
				if(m.isSufficientBalance(id_sender,amount))	//checks whether sender has sufficient amount or not
				{
					int sender_balance = (int)session.getAttribute("balance");
					session.removeAttribute("balance");	//removed so that new balance can be updated
		
					//System.out.println("sender_balance:"+sender_balance);
					
					//updating sender balance
					m.updateBalance(id_sender,sender_balance-amount);
					session.setAttribute("balance", "sender_balance-amount");	//new balance updated
					
					int receiver_balance = m.getBalance(id_receiver);
					
					//updating receiver balance
					m.updateBalance( id_receiver,receiver_balance+amount);
					
					
					//add the transaction record to database
					m.addRecord(id_receiver,amount,receiver_name,id_sender);
					
					session.setAttribute("success", "Transaction Successful");
					session.setAttribute("balance", sender_balance-amount);
					try {
						res.sendRedirect("/ABCBank/transfer.jsp");
					} catch (IOException e) {
						e.printStackTrace();
					}
					
				}
				else
				{
					System.out.println("Insufficient Balance!");
					session.setAttribute("error3","Insufficient Balance!" );
					try {
						res.sendRedirect("/ABCBank/transfer.jsp");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
			}
			else
			{
				System.out.println("Amount must be greater than 0!");
				session.setAttribute("error2","Amount must be greater than 0!" );
				try {
					res.sendRedirect("/ABCBank/transfer.jsp");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		else
		{
			System.out.println("Account doesn't exist!");
			session.setAttribute("error1","Account doesn't exist!" );
			try {
				res.sendRedirect("/ABCBank/transfer.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
	}

}
