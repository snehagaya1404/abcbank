package com.abc.model;
import java.sql.*;
import java.util.ArrayList;

import com.abc.transaction.Transaction;


public class Model {
	private int id;
	private String name;
	private String username;
	private String password;
	private int balance;
	

	String url = "jdbc:mysql://localhost:3306/abcbank";
	String uname = "root";
	String pass = "";
	Connection con=null ;
	
	
	public Model(){
		connection();
	}
	
	public int getBalance(){
		return balance;
	}
	
	public void setBalance(int balance){
		this.balance = balance;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public void connection()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(url,uname,pass);
		} catch (Exception e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		}
	}
	public boolean validLogin(){
		String query = "select * from registration where username = ? and password= ?";
		try {
			PreparedStatement pst = con.prepareStatement(query);
			pst.setString(1, username);
			pst.setString(2, password);
			ResultSet rs = pst.executeQuery();
			while(rs.next())
			{
				id = rs.getInt(1);	//set id of model.java to current person id
				balance = rs.getInt(5);
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public int getBalance(int id)
	{
		int bal=0;
		String query = "select balance from registration where id=?";
		try {
			PreparedStatement pst = con.prepareStatement(query);
			pst.setInt(1,id);
			ResultSet rs = pst.executeQuery();
			rs.next();
			bal = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return bal;
		
	}

	public boolean validateAccount(int id, String name) {
		
		String query = "select balance from registration where id=? and name=?";
		//int bal=0;
		
		try {
			PreparedStatement pst = con.prepareStatement(query);
			pst.setInt(1, id);
			pst.setString(2, name);
			ResultSet rs = pst.executeQuery();
			
			if(rs.next())
			{
				//bal = rs.getInt(1);
				//return bal+"";
				return true;
			}
			else
			{
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	public void updateBalance(int id,int balance) {
		
		System.out.println("in updateBalance");
		System.out.println("balance:"+balance);
		
		
		String query = "update registration set balance = ? where id=?";
		
		try {
			PreparedStatement pst = con.prepareStatement(query);
			pst.setInt(1, balance);
			pst.setInt(2, id);
			int i = pst.executeUpdate();
			System.out.println("Number of rows affected:"+i);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}

	public boolean isSufficientBalance(int id,int amount) {
		
		String query = "select balance from registration where id=?";
		
		try {
			PreparedStatement pst = con.prepareStatement(query);
			pst.setInt(1,id);
			ResultSet rs = pst.executeQuery();
			rs.next();
			int sender_bal = rs.getInt(1);
			if(amount<=sender_bal)
			{
				return true;
			}
			else
				return false;
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void setPassword(int id, String newpassword) {
		
		String query = "update registration set password=? where id=?";
		try {
			PreparedStatement pst = con.prepareStatement(query);
			pst.setString(1, newpassword);
			pst.setInt(2, id);
			pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String checkOldPassword(int id) {

		String query = "select password from registration where id=?";
		String s = "";
		try {
			PreparedStatement pst =  con.prepareStatement(query);
			pst.setInt(1, id);
			ResultSet rs = pst.executeQuery();
			rs.next();
			s = rs.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return s;
	}

	public void addRecord(int id_receiver, int amount, String receiver_name, int id_sender) {
		String query = "insert into transaction (transactionAccountNo,amount,transactionHolderName,accountNo) values(?,?,?,?);";
		
		try {
			PreparedStatement pst = con.prepareStatement(query);
			pst.setInt(1, id_receiver);
			pst.setInt(2, amount);
			pst.setString(3, receiver_name);
			pst.setInt(4, id_sender);
			
			pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public ArrayList<Transaction> getTransactionRecord(int id) {
		
		String query = "select * from transaction where accountNo=?";
		ArrayList<Transaction> al = new ArrayList<Transaction>();
		
		try {
			PreparedStatement pst = con.prepareStatement(query);
			pst.setInt(1, id);
			
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				Transaction t = new Transaction();
				t.setTransactionAccountNo(rs.getInt("transactionAccountNo"));
				t.setTransactionHolderName(rs.getString("transactionHolderName"));
				t.setAmount(rs.getInt("amount"));
				al.add(t);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return al;
		
	}

	

	
	
}
