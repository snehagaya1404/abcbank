package com.abc.transaction;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.abc.model.Model;


@SuppressWarnings("serial")
public class TransactionController extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
	{
		HttpSession session = req.getSession(false);
		Model m = new Model();
		int id = (int)session.getAttribute("id");
		
		ArrayList<Transaction> record= m.getTransactionRecord(id);
		session.setAttribute("record", record);
		try {
			res.sendRedirect("/ABCBank/transactionrecord.jsp");
		} catch (IOException e) {
			e.printStackTrace();
		}
		for(Transaction data: record){
			System.out.println(data.getTransactionAccountNo());
			System.out.println(data.getTransactionHolderName());
			System.out.println(data.getAmount());
		}
	}
}
