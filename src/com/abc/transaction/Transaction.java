package com.abc.transaction;

public class Transaction 
{
	String transactionHolderName;
	int transactionAccountNo,amount;
	
	public int getTransactionAccountNo() {
		return transactionAccountNo;
	}
	public void setTransactionAccountNo(int transactionAccountNo) {
		this.transactionAccountNo = transactionAccountNo;
	}
	public String getTransactionHolderName() {
		return transactionHolderName;
	}
	public void setTransactionHolderName(String transactionHolderName) {
		this.transactionHolderName = transactionHolderName;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {	
		this.amount = amount;
	}
	
	
	
}
